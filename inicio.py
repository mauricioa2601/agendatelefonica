from DataAccess.conexionMongo import ConexionMDB
from DataAccess.conexionMySQL import ConexionMySQL
from app.menu import MenuAgenda

print("\nBienvenido a su aplicacion de contactos")

option = input("\nEscriba 1 para usar MongoDB \nEscriba 2 para usar MySQL \n")

menuAgenda = MenuAgenda()

if option == 1:
    conexionMongo = ConexionMDB()
    print('Excelente opcion')
elif option == 2:
    conexionMySQL = ConexionMySQL()
    print('Excelente opcion')
try:
    option = True
except (option != 1, option != 2, ValueError) as error :
    print('No es una opcion valida')
finally:
    menuAgenda.mostrarMenu()
