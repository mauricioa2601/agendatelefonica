#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from app.agenda_telefonica import AgendaTelefonica
class OptionSwitcher:

    def __init__(self):
        self.contacto = AgendaTelefonica()

    def main(self, argument):
        method_name = 'option_' + str(argument)
        return getattr(self, method_name)

    def option_1(self,objBD):

        nombre = input('Nombre: ')
        telcelular = input('Telefono celular: ')
        telfijo = input('Telefono fijo: ')
        email = input('Email: ')
        fechanacimiento = input('Fecha de nacimiento : ')
        direccion = input(' Direccion : ')

        if(objBD == True):
            self.contacto.anadirSQL(nombre = nombre, telcelular = telcelular, telfijo = telfijo, email = email, fechanacimiento = fechanacimiento, direccion=direccion)
        else:
            self.contacto.anadirMongo(nombre = nombre, telcelular = telcelular, telfijo = telfijo, email = email, fechanacimiento = fechanacimiento, direccion=direccion)

    def option_2(self, objBD):
        contactos = self.contacto.listarSQL()
        print(json.dumps(contactos, indent=2))
        if(objBD == True):
            self.contacto.listarSQL()
        else:
            self.contacto.listarMongo()

