from app.option_switcher import OptionSwitcher
class MenuAgenda:

    def __init__(self):
        self.optionSwitcher = OptionSwitcher()
        #print('Metodo INIT de la clase MenuAgenda')

    def mostrarMenu(self):
        try:
            menu = [
                ['Agenda Personal'],
                ['----------------'],
                ['1. Añadir Contacto'],
                ['2. Lista de contactos'],
                ['3. Buscar contacto'],
                ['4. Editar contacto'],
                ['5. Eliminar contacto'],
                ['9. Cerrar agenda']
            ]

            print()
            print('----------------')
            for x in range(len(menu)):
                print(menu[x][0])

            print()
            option = int(input("Introduzca la opción deseada: "))
            self.validarOption(option)

        except ValueError:
            print()
            print('ERROR: Solo esta permitido el ingreso de numero....!!!')
            print()
            self.mostrarMenu()

    def validarOption(self, option):
        if option == 9:
            print("Saliendo de la agenda ...")
            exit()

        if option >=1  and option <=6:
            return option

        print()
        print("Opcion incorrecta. Intente nuevamente")
        print()
        self.mostrarMenu()

