from DataAccess.conexionMongo import ConexionMongo as mongo
from DataAccess.conexionMySql import ConexionMysql as mysql 

class Contacto():
    def main(self, argument):
        method_name = 'option_' + str(argument)
        return getattr(self, method_name)()

    def option_1(self):
        nombre = input('Primer Nombre : ')
        apellido = input('Apellido Paterno : ')
        telefono = input('Numero Telefono : ')
        email = input('Correo Electronico : ')
        dni = input('Documento DNI : ')

        resultado = self.añadirContacto(nombre = nombre, apellido = apellido, telefono = telefono, email = email, dni = dni)
        if resultado == True:
            print('Contacto añadido con exito.....!!!!!')

    def option_2(self):
        contactos = self.listarContacto()
        print(json.dumps(contactos, indent=2))

    def option_3(self):
        dni = input('Ingrese el DNI del contacto a buscar : ')
        contacto = self.buscarContacto(dni)
        if contacto is None:
            print ('Uhs el contacto buscado no existe')
        else:
            print(json.dumps(contacto, indent=2))

    def option_4(self):
        print('No hace nada la opcion numero 4')

    def option_5(self):
        dni = input('Ingrese el DNI del contacto a buscar : ')
        resultado = self.eliminarContacto(dni)
        if resultado is None:
            print ('Uhs el contacto que intenta eliminar no existe')
        else:
            print ('El contacto ha sido eliminado correctamente.....!!!! ')
