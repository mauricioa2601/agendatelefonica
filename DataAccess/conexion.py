import pymongo

class Conexion:

    def __init__(self, nombre, telcelular, telfijo, email, fechanacimiento, direccion):
        self.nombre = nombre
        self.telcelular = telcelular
        self.telfijo = telfijo
        self.email = email
        self.fechanacimiento = fechanacimiento
        self.direccion = direccion
        print('Metodo INIT de la clase AgendaTelefonica')

    def toDBCollection (self):
        return {
            'nombre':self.nombre,
            'telcelular':self.telcelular,
            'telfijo': self.telfijo,
            'email':self.email,
            'fechanacimiento':self.fechanacimiento,
            'direccion':self.direccion
        }

    def __str__(self):
        return "Nombre: %s - Telefono Celular: %i - Telefono Fijo: %i - E-mail: %s - Fecha de nacimiento: %s - Direccion %s" \
               %(self.nombre, self.telcelular, self.telfijo, self.email, self.fechanacimiento, self.direccion)
