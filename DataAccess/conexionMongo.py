# -*- coding: utf-8 -*-
import pymongo
from DataAccess.conexion import Conexion
class ConexionMDB:

    contactos = [
        Conexion('Javier', 987654321, 7654321, 'javier@gmail.com', '1990-10-10', 'Jr Las Taras 1010'),
        Conexion('Roxana', 987654322, 7654322, 'roxana@gmail.com', '1990-10-10', 'Jr Flores 1010'),
        Conexion('Mauricio', 987654323, 7654323, 'mauricio@gmail.com', '1990-10-10', 'Jr Cuba 1010'),
        Conexion('Pepe', 987654324, 7654324, 'pepe@gmail.com', '1990-10-10', 'Jr Las Otero 1010'),
        Conexion('Cielo', 987654325, 7654325, 'cielo@gmail.com', '1990-10-10', 'Av Alba 2020'),
        ]


    def __init__(self):
        myclient = pymongo.MongoClient('localhost',27017)
        db = myclient["agendatelefonica"]
        self.collection = db.contactos

        for contacto in self.contactos:
            self.collection.insert_one(contacto.toDBCollection())

        cursor = self.collection.find()
        for leer in cursor:
            print ("%s - %i - %i - %s - %s - %s" \
                %(leer['nombre'], leer['telcelular'], leer['telfijo'], leer['email'], leer['fechanacimiento'], leer['direccion']))

